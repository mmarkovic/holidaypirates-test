import axios from 'axios'

import dispatcher from '../dispatcher'
import ApiBase from '../ApiBase'

export function getHotels() {
  const hotelCount = 5

  dispatcher.dispatch({type: "FETCH_HOTELS"})
  
  axios.get(`${ApiBase}/hotels?count=${hotelCount}`)
    .then((response) => {
      dispatcher.dispatch({type: "RECEIVE_HOTELS", hotels: response.data})
    })
    .catch((error) => {
      dispatcher.dispatch({type: "FETCH_HOTELS_ERROR", error: error.response.data.error})
    })
}

export function selectHotel(hotel_id) {
  dispatcher.dispatch({type: "SELECT_HOTEL", hotel_id: hotel_id})
}