import React, { Component } from 'react'

import * as HotelActions from '../actions/HotelActions'
import HotelStore from '../stores/HotelStore'
import * as ReviewActions from '../actions/ReviewActions'
import ReviewStore from '../stores/ReviewStore'

import Hotel from '../components/Hotel'

export default class Main extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      hotels: [],
      loading: false,
      error: null,
      selectedHotel: null,
      reviews: [],
      reviewError: null,
    }
  }

  componentWillMount() {
    HotelStore.on("change", () => {
      this.setState({
        loading: HotelStore.getLoading(),
        hotels: HotelStore.getAll(),
        error: HotelStore.getError(),
        selectedHotel: HotelStore.getSelectedHotel(),
      })
    })
    ReviewStore.on("change", () => {
      this.setState({
        reviews: ReviewStore.getForHotel(),
        reviewError: ReviewStore.getError(),
      })
    })
  }

  selectHotel(hotel_id) {
    HotelActions.selectHotel(hotel_id)
    ReviewActions.getReviews(hotel_id)
  }

  loadHotels() {
    HotelActions.getHotels()
  }

  loadingAnimation() {
    return (
      <p>Amazen loading animation!!!</p>
    )
  }

  error() {
    return (
      <p className="well">{this.state.error}</p>
    )
  }

  makeHotelList() {
    const hotels = this.state.hotels.map((hotel) => {
      if (this.state.selectedHotel && hotel.id === this.state.selectedHotel.id) {
        return <Hotel key={hotel.id} hotel={hotel} reviews={this.state.reviews} reviewError={this.state.reviewError} selectHotel={this.selectHotel.bind(this)} />
      } else {
        return <Hotel key={hotel.id} hotel={hotel} selectHotel={this.selectHotel.bind(this)} />
      }
    })

    return (
      <ul>
        {hotels}
      </ul>
    )
  }

  render() {
    var content = null

    if (this.state.loading) {
      // Loading animation doesn't work for some reason, too tired to figure it out
      content = this.loadingAnimation()
    } else {
      if (this.state.hotels) {
        content = this.makeHotelList()
      }
      if (this.state.error) {
        content = this.error()
      }
    }

    return (
      <div className="container">
        <div className="load-button container">
          <button className="btn btn-default" onClick={this.loadHotels.bind(this)}>Load Hotels</button>
        </div>
        <div className="container main-content">
          {content}
        </div>
      </div>
    )
  }
}
