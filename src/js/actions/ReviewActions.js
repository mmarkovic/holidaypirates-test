import axios from 'axios'

import dispatcher from '../dispatcher'
import ApiBase from '../ApiBase'

export function getReviews(hotel_id) {
  dispatcher.dispatch({type: "FETCH_REVIEWS"})
  
  axios.get(`${ApiBase}/reviews?hotel_id=${hotel_id}`)
    .then((response) => {
      dispatcher.dispatch({type: "RECEIVE_REVIEWS", reviews: response.data})
    })
    .catch((error) => {
      dispatcher.dispatch({type: "FETCH_REVIEWS_ERROR", error: error.response.data.error})
    })
}