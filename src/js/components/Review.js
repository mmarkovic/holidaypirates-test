import React, { Component } from 'react'

class Review extends Component {
  render() {
    const {review} = this.props
    return (
      <li>
        {review.comment}
      </li>
    )
  }
}

export default Review