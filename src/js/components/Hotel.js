import React, { Component } from 'react'

import Review from './Review'

class Hotel extends Component {
  makeReviewList() {
    const reviewList = this.props.reviews.map((review) => {
      // Using Math.rand() because we need a unique identified and review doesn't have an id
      return <Review review={review} key={Math.random()} />
    })

    return (
      <ul>
        {reviewList}
      </ul>
    )
  }

  error() {
    return (
      <p className="well">{this.props.reviewError}</p>
    )
  }

  selectHotel() {
    this.props.selectHotel(this.props.hotel.id)
  }

  render() {
    const {hotel, reviews, reviewError} = this.props

    var content = null
    if (reviews) {
      content = this.makeReviewList()
    }
    if (reviewError) {
      content = this.error()
    }

    return (
      <li>
        {hotel.name}
        <button className="btn btn-default" onClick={this.selectHotel.bind(this)}>Show reviews</button>
        {content}
      </li>
    )
  }
}

export default Hotel
