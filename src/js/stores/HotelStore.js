import { EventEmitter } from 'events'
import _ from 'lodash'

import dispatcher from '../dispatcher'

class HotelStore extends EventEmitter {
  constructor() {
    super();
    this.loading = false
    this.hotels = []
    this.error = null
    this.selectedHotel = null
  }

  getAll() {
    return this.hotels
  }

  getLoading() {
    return this.loading
  }

  getError() {
    return this.error
  }

  getSelectedHotel() {
    return this.selectedHotel
  }

  handleActions(action) {
    switch(action.type) {
      case "FETCH_HOTELS": {
        this.loading = true
        this.emit('change')
      }
      case "RECEIVE_HOTELS": {
        this.hotels = action.hotels
        this.loading = false
        this.emit('change')
      }
      case "FETCH_HOTELS_ERROR": {
        this.error = action.error
        this.loading = false
        this.emit('change')
      }
      case "SELECT_HOTEL": {
        var hotel = _.find(this.hotels, {id: action.hotel_id})
        this.selectedHotel = hotel
        this.emit('change')
      }
    }
  }
}

const hotelStore = new HotelStore
dispatcher.register(hotelStore.handleActions.bind(hotelStore))

export default hotelStore