import { EventEmitter } from 'events'

import dispatcher from '../dispatcher'

class ReviewStore extends EventEmitter {
  constructor() {
    super();
    this.reviews = []
    this.error = null
  }

  getForHotel() {
    return this.reviews
  }

  getError() {
    return this.error
  }

  handleActions(action) {
    switch(action.type) {
      case "FETCH_REVIEWS": {
      }
      case "RECEIVE_REVIEWS": {
        this.reviews = action.reviews
        this.emit('change')
      }
      case "FETCH_REVIEWS_ERROR": {
        this.error = action.error
        this.emit('change')
      }
    }
  }
}

const reviewStore = new ReviewStore
dispatcher.register(reviewStore.handleActions.bind(reviewStore))

export default reviewStore